import server from "./server";
import TestController from './controllers/v1/TestController';
import { v4 as uuidv4 } from 'uuid';

const port = process.env.PORT || 3000;
const appKey = process.env.APP_KEY || uuidv4(); 


server(appKey).then(app => {

    app.use('/api/v1', TestController);

    app.listen(port, function () {
        console.log(`Server listen on port ${port}`);
    });
});
