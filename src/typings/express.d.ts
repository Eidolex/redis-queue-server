
import QueueEventService from "../services/QueueEventService";
import TaskService from "../services/TaskService";

declare module 'express-serve-static-core' {
    interface Request {
        taskService: TaskService
        queueEventService: QueueEventService,
    }
}