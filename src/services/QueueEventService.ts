import { EventEmitter } from "events";

export default class QueueEventService {
    private readonly doneLists: { [key: string]: any };
    private readonly eventLists: { [key: string]: any };
    private readonly event: EventEmitter;

    constructor() {
        this.eventLists = {};
        this.doneLists = {};
        this.event = new EventEmitter();
    }

    public done(name: string, jobId: number) {
        console.log(`JobID: ${jobId}, Job: ${name} is done`)
        if (this.eventLists[name]) {
            if (this.doneLists[name]) {
                const keys = Object.keys(this.doneLists[name]);
                for (let key of keys) {
                    delete this.eventLists[name][key]
                }
                delete this.doneLists[name];
            }
            if (this.eventLists[name][jobId]) {
                delete this.eventLists[name][jobId];
            }
            if (Object.keys(this.eventLists[name]).length === 0) {
                this.event.emit(name);
            }
        } else {
            this.doneLists[name] = {
                ...this.doneLists[name],
                jobId: true,
            }
        }
    }


    public listen(name: string, jobIds: (number | string)[], callback: () => void) {
        if (this.eventLists[name]) {
            throw new Error("event already exist");
        }
        this.eventLists[name] = jobIds.reduce((result, pre) => {
            return {
                ...result,
                [pre]: true,
            };
        }, {});
        const listener = () => {
            callback();
            this.event.removeListener(name, listener);
        }
        this.event.on(name, listener);
    }
}