import Redis from 'ioredis';
import BQueue, { Queue, Job } from 'bull';
import { v4 as uuidv4 } from 'uuid';

export default class TaskService{

    private readonly key: string;
    private readonly subscriber: Redis.Redis;
    private readonly queue: Queue;

    constructor(key: string, queueName: string) {
        const subscriber = new Redis();
        const opts = {
            createClient: function (type: string) {
                if (type === 'subscriber') {
                    return subscriber;
                } else {
                    return new Redis();
                }
            }
        };
        this.key = key;
        this.subscriber = subscriber;
        this.queue = new BQueue(queueName, opts);
    }

    public async init() {
        await this.subscriber.subscribe(this.key);
    }

    public async listen(listener: (...args: any[]) => void) {
        this.subscriber.on('message', listener)
    }

    public async newTask(data: any, uniqueKey?: string): Promise<Job> {
        if(typeof data !== 'object')
            throw new Error('the data can only be object');

        uniqueKey = uniqueKey || uuidv4();
        data = {
            ...data,
            uniqueKey,
            appKey: this.key,
        }
        return this.queue.add(data);
    }

    public async newTasks(data: any[], uniqueKey?: string): Promise<Job[]> {
        if(!Array.isArray(data))
            throw new Error('the data can only be array');
        if(data.length === 0)
            throw new Error("the data can't be empty");

        uniqueKey = uniqueKey || uuidv4();

        return Promise.all(data.map((d) => this.newTask(d, uniqueKey)));
    }
}