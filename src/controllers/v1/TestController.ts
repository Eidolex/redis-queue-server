import { Request, Response, Router } from 'express';

const router = Router();


router.get('/test', async (req: Request, res: Response) => {
    const {id, data: { uniqueKey }} = await req.taskService.newTask({do: 1});
    process.nextTick(() => {
        req.queueEventService.listen(uniqueKey, [id], () => {
            res.send("done");
        })
    });
});

export default router;