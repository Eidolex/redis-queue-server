import express, { NextFunction, Request, Response } from 'express';
import QueueEventService from './services/QueueEventService';
import TaskService from './services/TaskService';

type JobResponse = {
    uniqueKey: string,
    jobId: number,
};


const server = async (appKey: string) => {
    const queueEventService = new QueueEventService();
    const taskService = new TaskService(appKey, 'fcm queue');

    await taskService.init();

    taskService.listen((_, message) => {
        const { uniqueKey, jobId }: JobResponse = JSON.parse(message);
        queueEventService.done(uniqueKey, jobId);
    });

    const queues = async (req: Request, _: Response, next: NextFunction) => {
        req.taskService = taskService;
        req.queueEventService = queueEventService;
        next();
    };

    const app = express();

    app.use(queues);

    return app;
};

export default server;