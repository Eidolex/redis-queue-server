# Redis Queue Server

Simple example creating queue server with node and redis

## Prerequisites

Please make sure you had install and running redis on your machine


## Installation

If you don't had yarn package manager please install [yarn](https://classic.yarnpkg.com/en/docs/install) 

And run the command below

```bash
yarn install
```

## Build and start server

Please run the below command step by step in the root of the project

```bash
yarn build
```

```bash
yarn start
```

## Running the example

Before running the example please make sure [redis-queue-listener](https://bitbucket.org/Eidolex/redis-queue-listener/src) is running.

And then request the url http://localhost:3000/api/v1/test

